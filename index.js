const fs = require("fs");
const fetch = require("node-fetch");
const sync = require("child_process").spawnSync;
const simpleGit = require("simple-git");
// Initialize config
const data = fs.readFileSync("config.json");
const config = JSON.parse(data);
const git = simpleGit({
  baseDir: config.gitDir,
});

// Declare function to push files to git
let pushLang = () => {
  git
    .outputHandler((command, stdout, stderr) => {
      stdout.pipe(process.stdout);
      stderr.pipe(process.stderr);
      stderr.on('data', (data) => {
        if (data.toString().includes('pathspec')) {
          clearInterval(timer);
          process.exit();
        }
      })
    })
    .status()
    .then((status) => {
      console.log(status.current);
      if (status.current.includes(config.branch)) {
        console.log("Found it!!");
      } else {
        git.checkout(config.branch);
      }
    })
    .then(() => {
      git
        .fetch()
        .pull({ "--ff-only": null })
        .then((pull) => {
          pullLang();
        })
        .then(() => {
          git.add(config.output).commit("Update language files").push();
        });
    });
};

// Declare function to pull phraser language files
let pullLang = () => {
  const cli = sync(
    config.command,
    ["pull", "--config", config.phraseConfigFile],
    {
      cwd: config.output,
      stdio: "pipe",
    }
  );
  let savedOutput = cli.stdout.toString();
  let savedOutputErr = cli.stderr.toString();
  if (savedOutputErr != "") {
    console.log(savedOutputErr);
    clearInterval(timer);
    process.exit();
  } else {
    console.log(savedOutput);
  }
};

// Checks if the config is set properly
const pathsToCheck = [config.output, config.phraseConfigFile, config.gitDir];
for (let i = 0; i < pathsToCheck.length; i++) {
  fs.accessSync(pathsToCheck[i], fs.constants.F_OK);
}

console.log("Program is up running");

//Enable timer to pull the languages and push them to git
var timer = setInterval(versionLoop, 60000);
function versionLoop() {
  fetch("https://rc.nl.carsys.online/version.json")
    .then((response) => response.json())
    .then((data) => {
      if (typeof ver1 === "undefined" && typeof ver2 === "undefined") {
        ver1 = data;
        ver2 = data;
      } else if (ver1["Commit id"] !== ver2["Commit id"]) {
        ver1 = data;
        ver2 = data;
        pushLang()
      } else {
        ver2 = data;
      }
    });
}
